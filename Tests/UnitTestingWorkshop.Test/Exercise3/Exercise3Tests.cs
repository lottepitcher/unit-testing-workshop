﻿using Moq;
using NUnit.Framework;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using UnitTestingWorkshop.BusinessLogic.Services;

namespace UnitTestingWorkshop.Test.Exercise3
{
    [TestFixture]
    public class Exercise3Tests
    {
        IPostService _postService;
        private Mock<IContentService> _contentServiceMock;

        const int MemberId = 123;
        const int ParentNodeId = 999;
        const string TopicTitle = "some topic title";
        const string TopicBody = "some topic body";


        [SetUp]
        public void Setup()
        {
            _contentServiceMock = new Mock<IContentService>();
            _postService = new PostService(_contentServiceMock.Object);
        }



        [Test]
        public void Create_CreatePost_Calls_Save_And_Publish()
        {
            var postMock = new Mock<IContent>();

            // set the fake ContentService to return my post mock, when given these specific parameters 
            _contentServiceMock.Setup(x => x.CreateContent(TopicTitle, ParentNodeId, "Post", 0)).Returns(postMock.Object);

            var result = _postService.CreateTopic(TopicTitle, TopicBody, ParentNodeId, MemberId);

            // assert that the result was "success"
            Assert.AreEqual("success", result);

            // verify that the methods Save and Publish were called in the fake ContentService 
            _contentServiceMock.Verify(x => x.Save(postMock.Object, 0, true));
            _contentServiceMock.Verify(x => x.Publish(postMock.Object, 0));
        }



        [Test]
        public void Create_CreateTopic_Does_Not_Create_Post_When_Topic_Title_Empty()
        {
            // TODO: Finish me!

        }



    }
}
